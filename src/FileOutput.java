import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileOutput {
	public static void writeFile(Solutions sol, String fileName) throws IOException {
		String line = "trans_code,vehicle_type,dist_seq,distribute_lea_tm,distribute_arr_tm,distance,trans_cost,charge_cost,wait_cost,fixed_use_cost,total_cost,charge_cnt\n";
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false));
		writer.write(line);
		for (Solution s : sol.solutions) {
			writer.write(s.toFileString());
		}
		writer.close();
		System.out.println("output OK");
	}

	public static String transCode(int carNum) {
		String num = Integer.toString(carNum);
		String s = "DP";
		for (int i = num.length(); i < 4; i++) {
			s += "0";
		}
		s += num;
		return s;
	}
}
