

public class IODistTime {
	public int[][] distances;
	public int[][] times;
	public IODistTime(int n) {
		this.distances = new int[n][n];
		this.times = new int[n][n];
	}
	public int getDistance(int fromId, int toId) {
		return distances[fromId][toId];
	}
	public int getTime(int fromId, int toId) {
		return times[fromId][toId];
	}
}
