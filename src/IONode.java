

public class IONode {
	public int id;
	public int type;
	public double lat;
	public double lng;
	public double pack_total_weight;
	public double pack_total_volume;
	public int first_receive_tm;
	public int last_receive_tm;

	public IONode(int id, int type, double lat, double lng, double weight, double volume, String first, String last) {
		this.id = id;
		this.type = type;
		this.lat = lat;
		this.lng = lng;
		this.pack_total_weight = weight;
		this.pack_total_volume = volume;
		this.first_receive_tm = IOTimeInt.Time2Int(first);
		this.last_receive_tm = IOTimeInt.Time2Int(last);
	}
}
