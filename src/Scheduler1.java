import java.util.ArrayList;
import java.util.List;

// 最多只允许充电一次，全排列算法

public class Scheduler1 extends Scheduler {
	private IOVehicle vehicle;
	private List<IONode> smallPois;
	private int carNum;
	private PermInfo optInfo;
	
	@Override
	public Solution schedule(List<IONode> pois, IOVehicle vehicle, int carNum) {
		this.vehicle = vehicle;
		this.smallPois = pois;
		this.carNum = carNum;
		if (pois.size() > 4) {
			return null;
		}
		PermInfo initInfo = init();
		optInfo = initInfo.clone();
		traversal0(0, initInfo);
		if (optInfo.sol.total_cost < Double.MAX_VALUE) {
			return optInfo.sol;
		}
		initInfo = init();
		traversal1(0, initInfo);
		if (optInfo.sol.total_cost < Double.MAX_VALUE) {
			return optInfo.sol;
		}
		return null;
	}
	
	private PermInfo init() {
		PermInfo initInfo = new PermInfo();
		initInfo.perm = new int[smallPois.size()];
		for (int i = 0; i < smallPois.size(); i++) {
			initInfo.perm[i] = i;
		}
		initInfo.sol.total_cost = Double.MAX_VALUE;
		return initInfo;
	}

	private class PermInfo {
		int[] perm;
		Solution sol = new Solution();
		public PermInfo clone() {
			PermInfo pinfo = new PermInfo();
			pinfo.perm = this.perm.clone();
			pinfo.sol = this.sol.clone();
			return pinfo;
		}
	}
	
	private void traversal0(int n, PermInfo info) {
		if (n == smallPois.size() - 1) {
			info.sol.seq = formSeq0(info.perm);
			info.sol.constructRoute(carNum, vehicle);
			if (info.sol.valid && optInfo.sol.total_cost > info.sol.total_cost) {
				optInfo = info.clone();
			}
		} else {
			for (int i = n; i < smallPois.size(); i++) {
				swap(n, i, info);
				traversal0(n + 1, info);
				swap(n, i, info);
			}
		}
	}
	
	private void traversal1(int n, PermInfo info) {
		if (n == smallPois.size() - 1) {
			for (int j = 0; j < smallPois.size(); j++) {
				info.sol.seq = formSeq1(info.perm, j);
				info.sol.constructRoute(carNum, vehicle);
				if (info.sol.valid && optInfo.sol.total_cost > info.sol.total_cost) {
					optInfo = info.clone();
				}
			}
		} else {
			for (int i = n; i < smallPois.size(); i++) {
				swap(n, i, info);
				traversal1(n + 1, info);
				swap(n, i, info);
			}
		}
	}

	private List<Integer> formSeq1(int[] perm, int j) {
		List<Integer> seq = new ArrayList<>();
		seq.add(0);
		int minDist = Integer.MAX_VALUE;
		int minId = -1;
		int prevDist = 0, afterDist = 0;
		for (int i = 0; i <= j; i++) {
			int seqId = smallPois.get(perm[i]).id;
			int prevId = (i == 0) ? 0 : smallPois.get(perm[i - 1]).id;
			int step = FileInput.distanceTimes.getDistance(prevId, seqId);
			prevDist += step;
		}
		for (int i = j + 1; i < perm.length; i++) {
			int seqId = smallPois.get(perm[i]).id;
			int afterId = (i == perm.length - 1) ? 0 : smallPois.get(perm[i + 1]).id;
			int step = FileInput.distanceTimes.getDistance(seqId, afterId);
			afterDist += step;
		}
		for (int i = 0; i < perm.length; i++) {
			seq.add(smallPois.get(perm[i]).id);
			if (i == j) {
				for (IONode charger : FileInput.nodes.chargers) {
					prevDist += FileInput.distanceTimes.getDistance(smallPois.get(perm[i]).id, charger.id);
					if (prevDist > vehicle.driving_range) {
						continue;
					}
					int nextId = (i < perm.length - 1) ? smallPois.get(perm[i + 1]).id : 0;
					afterDist += FileInput.distanceTimes.getDistance(nextId, charger.id);
					if (afterDist > vehicle.driving_range) {
						continue;
					}
					int allDist = prevDist + afterDist;
					if (allDist < minDist) {
						minDist = allDist;
						minId = charger.id;
					}
				}
				if (minId > 0) {
					seq.add(minId);
				}
			}
		}
		seq.add(0);
		return seq;
	}

	private List<Integer> formSeq0(int[] perm) {
		List<Integer> seq = new ArrayList<>();
		seq.add(0);
		for (int i = 0; i < perm.length; i++) {
			seq.add(smallPois.get(perm[i]).id);
		}
		seq.add(0);
		return seq;
	}

	private void swap(int i, int j, PermInfo pinfo) {
		int buf;
		buf = pinfo.perm[i];
		pinfo.perm[i] = pinfo.perm[j];
		pinfo.perm[j] = buf;
	}
}
