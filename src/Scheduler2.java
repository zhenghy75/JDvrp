import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Scheduler2 extends Scheduler {
	private IONodes nodes;
	private IODistTime distTimes;
	private List<IONode> smallPois;
	private IOVehicle vehicle;

	@Override
	public Solution schedule(List<IONode> pois, IOVehicle vehicle, int carNum) {
		this.nodes = FileInput.nodes;
		this.distTimes = FileInput.distanceTimes;
		this.vehicle = vehicle;
		this.smallPois = pois;
		sortPois();
		int startTime = getStartTime();
		Solution sol = new Solution();
		sol.seq = shortestRoute(startTime);
		if (sol.seq == null) {
			return null;
		}
		sol.constructRoute(carNum, vehicle);
		return sol;
	}

	private void sortPois() {
		Collections.sort(smallPois, new Comparator<IONode>() {
			@Override
			public int compare(IONode o1, IONode o2) {
				return Integer.compare(o1.last_receive_tm, o2.last_receive_tm);
			}
		});
	}

	private int getStartTime() {
		int centerNodeTime = distTimes.getTime(0, smallPois.get(0).id);
		int startTime = smallPois.get(0).first_receive_tm - centerNodeTime;
		startTime = (startTime >= 480) ? startTime : 480;
		return startTime;
	}

	private List<Integer> shortestRoute(int startTime) {
		int n = smallPois.size();
		boolean[] isScheduled = new boolean[n];
		for (int i = 0; i < n; i++) {
			isScheduled[i] = false;
		}
		IONode thisNode = nodes.center;
		List<Integer> seq = new ArrayList<>();
		seq.add(0);
		int endTime = startTime;
		int remainDist = vehicle.driving_range;
		while (!allScheduled(isScheduled)) {
			int minDist = Integer.MAX_VALUE;
			IONode minNode = null;
			int minIndex = -1;
			for (int i = 0; i < n; i++) {
				if (isScheduled[i]) {
					continue;
				}
				int arriveTime = endTime + distTimes.getTime(thisNode.id, smallPois.get(i).id);
				if (arriveTime > smallPois.get(i).last_receive_tm) {
					continue;
				}
				int thisDist = distTimes.getDistance(thisNode.id, smallPois.get(i).id);
				if (thisDist < minDist) {
					minDist = thisDist;
					minNode = smallPois.get(i);
					minIndex = i;
				}
			}
			if (minIndex >= 0) {
				isScheduled[minIndex] = true;
				if (minDist > remainDist) {
					IONode charger = findCharger(thisNode, minNode, remainDist);
					if (charger == null) {
						return null;
					}
					seq.add(charger.id);
					endTime += distTimes.getTime(thisNode.id, charger.id) + 30;
					endTime += distTimes.getTime(charger.id, minNode.id);
					if (endTime < minNode.first_receive_tm) {
						endTime = minNode.first_receive_tm;
					}
					endTime += 30;
					remainDist = vehicle.driving_range - distTimes.getDistance(charger.id, minNode.id);
				} else {
					remainDist -= minDist;
					endTime += distTimes.getTime(thisNode.id, minNode.id);
					if (endTime < minNode.first_receive_tm) {
						endTime = minNode.first_receive_tm;
					}
					endTime += 30;
				}
				thisNode = minNode;
				seq.add(minNode.id);
			} else {
				return null;
			}
		}
		seq.add(0);
		return seq;
	}

	private IONode findCharger(IONode thisNode, IONode minNode, int remainDist) {
		int minDist = Integer.MAX_VALUE;
		IONode minCharger = null;
		for (IONode charger : nodes.chargers) {
			int dist = distTimes.getDistance(thisNode.id, charger.id);
			if (dist > remainDist) {
				continue;
			}
			dist += distTimes.getDistance(charger.id, minNode.id);
			if (dist < minDist) {
				minDist = dist;
				minCharger = charger;
			}
		}
		return minCharger;
	}

	private boolean allScheduled(boolean[] isScheduled) {
		for (int i = 0; i < isScheduled.length; i++) {
			if (!isScheduled[i]) {
				return false;
			}
		}
		return true;
	}

}
