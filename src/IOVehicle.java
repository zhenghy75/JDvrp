

public class IOVehicle {
	public int id;
	public String name;
	public double max_volume;
	public double max_weight;
	public int vehicle_cnt;
	public int driving_range;
	public double charge_tm;
	public double unit_trans_cost;
	public int vehicle_cost;

	public IOVehicle(int id, String name, double volume, double weight, int cnt, int range, double charge, double trans,
			int vehicle) {
		this.id = id;
		this.name = name;
		this.max_volume = volume;
		this.max_weight = weight;
		this.vehicle_cnt = cnt;
		this.driving_range = range;
		this.charge_tm = charge;
		this.unit_trans_cost = trans;
		this.vehicle_cost = vehicle;
	}
}
