import java.util.ArrayList;
import java.util.List;

public class SolverSimple {
	private IONodes nodes;
	private IODistTime distanceTimes;

	public Solutions calculate() {
		this.nodes = FileInput.nodes;
		this.distanceTimes = FileInput.distanceTimes;

		Solutions sols = new Solutions();
		int carNum = 0;
		for (IONode poi : nodes.pois) {
			carNum++;
			IOVehicle vehicle = (overload(poi)) ? FileInput.vehicles.truck : FileInput.vehicles.iveco;
			int chargerId = charger(poi.id, vehicle.driving_range);
			Solution sol = new Solution();
			sol.seq = formSeq(poi.id, chargerId);
			sol.constructRoute(carNum, vehicle);
			sols.solutions.add(sol);
		}
		return sols;
	}

	private boolean overload(IONode poi) {
		if (poi.pack_total_volume > FileInput.vehicles.iveco.max_volume) {
			return true;
		}
		if (poi.pack_total_weight > FileInput.vehicles.iveco.max_weight) {
			return true;
		}
		return false;
	}

	private List<Integer> formSeq(int poiId, int chargerId) {
		List<Integer> seq = new ArrayList<>();
		seq.add(0);
		seq.add(poiId);
		if (chargerId > 0) {
			seq.add(chargerId);
		}
		seq.add(0);
		return seq;
	}

	private int charger(int nodeId, int vehicleMax) {
		int centerNodeDist = distanceTimes.getDistance(nodes.center.id, nodeId);
		if (centerNodeDist * 2 <= vehicleMax) {
			return 0;
		} else {
			int minDist = Integer.MAX_VALUE;
			int minChargerId = 0;
			for (IONode charger : nodes.chargers) {
				if (distanceTimes.getDistance(nodeId, charger.id) + centerNodeDist > vehicleMax) {
					continue;
				}
				int newDist = distanceTimes.getDistance(nodeId, charger.id)
						+ distanceTimes.getDistance(charger.id, nodes.center.id);
				if (newDist < minDist) {
					minDist = newDist;
					minChargerId = charger.id;
				}
			}
			return minChargerId;
		}
	}
}