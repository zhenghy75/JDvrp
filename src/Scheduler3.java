import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Scheduler3 extends Scheduler {
	private IONodes nodes;
	private IODistTime distTimes;
	private List<IONode> smallPois;
	private IOVehicle vehicle;

	@Override
	public Solution schedule(List<IONode> pois, IOVehicle vehicle, int carNum) {
		this.nodes = FileInput.nodes;
		this.distTimes = FileInput.distanceTimes;
		this.vehicle = vehicle;
		this.smallPois = pois;
		sortPois();
		int startTime = getStartTime();
		Solution sol = new Solution();
		List<IONode> shortest = shortestRoute(startTime);
		if (shortest == null) {
			return null;
		}
		List<IONode> opt = twoOpt(shortest);
		List<Integer> seq = toSeq(opt);
		if (seq == null) {
			return null;
		}
		sol.seq = seq;
		sol.constructRoute(carNum, vehicle);
		return sol;
	}

	private List<Integer> toSeq(List<IONode> route) {
		List<Integer> seq = new ArrayList<>();
		int remainDist = vehicle.driving_range;
		for (int i = 0; i < route.size() - 1; i++) {
			IONode thisNode = route.get(i);
			seq.add(thisNode.id);
			IONode nextNode = route.get(i + 1);
			int dist = distTimes.getDistance(thisNode.id, nextNode.id);
			if (dist > remainDist) {
				IONode charger = findCharger(thisNode, nextNode, remainDist);
				if (charger == null) {
					return null;
				}
				seq.add(charger.id);
				remainDist = vehicle.driving_range - distTimes.getDistance(charger.id, nextNode.id);
			} else {
				remainDist -= dist;
			}
		}
		seq.add(0);
		return seq;
	}

	private IONode findCharger(IONode thisNode, IONode minNode, int remainDist) {
		int minDist = Integer.MAX_VALUE;
		IONode minCharger = null;
		for (IONode charger : nodes.chargers) {
			int dist = distTimes.getDistance(thisNode.id, charger.id);
			if (dist > remainDist) {
				continue;
			}
			dist += distTimes.getDistance(charger.id, minNode.id);
			if (dist < minDist) {
				minDist = dist;
				minCharger = charger;
			}
		}
		return minCharger;
	}

	private void sortPois() {
		Collections.sort(smallPois, new Comparator<IONode>() {
			@Override
			public int compare(IONode o1, IONode o2) {
				return Integer.compare(o1.last_receive_tm, o2.last_receive_tm);
			}
		});
	}

	private int getStartTime() {
		int centerNodeTime = distTimes.getTime(0, smallPois.get(0).id);
		int startTime = smallPois.get(0).first_receive_tm - centerNodeTime;
		startTime = (startTime >= 480) ? startTime : 480;
		return startTime;
	}

	private List<IONode> shortestRoute(int startTime) {
		int n = smallPois.size();
		boolean[] isScheduled = new boolean[n];
		for (int i = 0; i < n; i++) {
			isScheduled[i] = false;
		}
		IONode thisNode = nodes.center;
		List<IONode> seq = new ArrayList<>();
		seq.add(nodes.center);
		while (!allScheduled(isScheduled)) {
			int minDist = Integer.MAX_VALUE;
			IONode minNode = null;
			int minIndex = -1;
			for (int i = 0; i < n; i++) {
				if (isScheduled[i]) {
					continue;
				}
				int thisDist = distTimes.getDistance(thisNode.id, smallPois.get(i).id);
				if (thisDist < minDist) {
					minDist = thisDist;
					minNode = smallPois.get(i);
					minIndex = i;
				}
			}
			if (minIndex >= 0) {
				isScheduled[minIndex] = true;
				thisNode = minNode;
				seq.add(minNode);
			} else {
				return null;
			}
		}
		seq.add(nodes.center);
		return seq;
	}

	private List<IONode> twoOpt(List<IONode> opt) {
		final int n = opt.size();
		List<IONode> searchPerm = copyPerm(opt);
		List<IONode> tempPerm = new ArrayList<>(opt.size());
		double optDist = totalDist(opt);
		for (int i = 1; i < n - 2; i++) {
			for (int j = i + 1; j < n - 2; j++) {
				tempPerm = inverse(searchPerm, i, j);
				double tempDist = totalDist(tempPerm);
				if (tempDist < optDist) {
					optDist = tempDist;
					searchPerm = copyPerm(tempPerm);
				}
			}
		}
		return searchPerm;
	}

	List<IONode> copyPerm(List<IONode> a) {
		List<IONode> b = new ArrayList<>();
		for (IONode node : a) {
			b.add(node);
		}
		return b;
	}

	int totalDist(List<IONode> opt) {
		int dist = 0;
		for (int i = 0; i < opt.size() - 1; i++) {
			IONode thisNode = opt.get(i);
			IONode nextNode = opt.get(i + 1);
			dist += distTimes.getDistance(thisNode.id, nextNode.id);
		}
		return dist;
	}

	private List<IONode> inverse(List<IONode> perm, int a, int b) {
		List<IONode> tempPerm = new ArrayList<>(perm.size());
		tempPerm = copyPerm(perm);
		List<IONode> temp = new ArrayList<>(b - a + 1);
		for (int i = b; i >= a; i--) {
			temp.add(tempPerm.get(i));
		}
		for (int i = 0; i < b - a + 1; i++) {
			tempPerm.set(a + i, temp.get(i));
		}
		return tempPerm;
	}

	private boolean allScheduled(boolean[] isScheduled) {
		for (int i = 0; i < isScheduled.length; i++) {
			if (!isScheduled[i]) {
				return false;
			}
		}
		return true;
	}

}
