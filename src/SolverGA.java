import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SolverGA {
	public static final int AGENT_NUM = 100;
	public static final int ITER = 50;
	public static final int KEEP = 20;
	public static final double BETA = 0.3;
	
	private Solutions sols;
	
	private class Agent {
		public Solutions solutions = sols.clone();
		public List<Double> orderList = new ArrayList<>(1000);
		
		public void initRandom() {
			for (int i = 0; i < 1000; i++) {
				double e = Math.random();
				orderList.add(e);
			}
		}
	}
	
	public void setOrder(Solutions solutions, List<Double> orderList) {
		for (int i = 0; i < 1000; i++) {
			solutions.solutions.get(i).order = orderList.get(i);
		}
	}
	
	public Solutions calculate(Solutions sols) {
		SolverMerge solverMerge = new SolverMerge();
		this.sols = sols.clone();
		List<Agent> agents = init();
		Agent bestAgent = agents.get(0);
		for (int i = 0; i < ITER; i++) {
			System.out.println("i = " + i);
			for (int j = 0; j < AGENT_NUM; j++) {
				Solutions newSols = this.sols.clone();
				setOrder(newSols, agents.get(j).orderList);
				agents.get(j).solutions = solverMerge.calculate(newSols, 1, false);
				System.out.println("j = " + j + ", cost = " + agents.get(j).solutions.totalCost());
				Collections.sort(agents, new Comparator<Agent>() {
					@Override
					public int compare(Agent o1, Agent o2) {
						return Double.compare(o1.solutions.totalCost(), o2.solutions.totalCost());
					}
				});
			}
			if (agents.get(0).solutions.totalCost() < bestAgent.solutions.totalCost()) {
				bestAgent = agents.get(0);
			}
			System.out.println("best = " + bestAgent.solutions.totalCost());
			for (int j = KEEP; j < AGENT_NUM; j++) {
				agents.get(j).initRandom();
			}
			for (int j = 0; j < AGENT_NUM; j++) {
				for (int k = 0; k < 1000; k++) {
					double prev = agents.get(j).orderList.get(k);
					double bestPrev = bestAgent.orderList.get(k);
					double after = BETA * bestPrev + (1 - BETA) * prev;
					agents.get(j).orderList.set(k, after);
				}
			}
		}
		return bestAgent.solutions;
	}
	
	private List<Agent> init() {
		List<Agent> agents = new ArrayList<>();
		for (int i = 0; i < AGENT_NUM; i++) {
			Agent a = new Agent();
			a.initRandom();
			agents.add(a);
		}
		return agents;
	}
}
