import java.util.ArrayList;

public class SolverLink {
	private Solutions linkedSols;

	public Solutions calculate(Solutions sols) {
		int n = sols.solutions.size();
		boolean[] isLinked = new boolean[n];
		for (int i = 0; i < n; i++) {
			isLinked[i] = false;
		}
		this.linkedSols = new Solutions();
		while (true) {
			double maxImprovement = -1;
			int maxAIndex = -1, maxBIndex = -1;
			Solution maxSol = null;
			for (int i = 0; i < n; i++) {
				Solution a = sols.solutions.get(i);
				if (isLinked[i]) {
					continue;
				}
				for (int j = 0; j < n; j++) {
					if (isLinked[j]) {
						continue;
					}
					if (j == i) {
						continue;
					}
					Solution b = sols.solutions.get(j);
					Solution linked = linkSols(a, b);
					if (linked == null) {
						continue;
					}
					double improvement = a.total_cost + b.total_cost - linked.total_cost;
					if (improvement < 0) {
						continue;
					}
					if (improvement > maxImprovement) {
						maxImprovement = improvement;
						maxAIndex = i;
						maxBIndex = j;
						maxSol = linked;
					}
				}
			}
			if (maxImprovement <= 0.01) {
				break;
			}
			linkedSols.solutions.add(maxSol);
			isLinked[maxAIndex] = true;
			isLinked[maxBIndex] = true;
		}
		for (int i = 0; i < n; i++) {
			if (!isLinked[i]) {
				linkedSols.solutions.add(sols.solutions.get(i));
			}
		}
		transCode();
		return this.linkedSols;
	}

	private void transCode() {
		int i = 0;
		for (Solution sol : linkedSols.solutions) {
			i++;
			sol.trans_code = FileOutput.transCode(i);
		}
	}

	private Solution linkSols(Solution a, Solution b) { // a早 b晚
		int aArr = IOTimeInt.Time2Int(a.distribute_arr_tm);
		int bLea = IOTimeInt.Time2Int(b.distribute_lea_tm);
		if (aArr + 60 > bLea) {
			return null;
		}
		if (a.vehicle_type != b.vehicle_type) {
			return null;
		}
		Solution linked = new Solution();
		linked.vehicle_type = a.vehicle_type;
		linked.seq = new ArrayList<>();
		for (int i = 0; i < a.seq.size(); i++) {
			linked.seq.add(a.seq.get(i));
		}
		for (int i = 1; i < b.seq.size(); i++) {
			linked.seq.add(b.seq.get(i));
		}
		linked.dist_seq = linked.distSeq(linked.seq);
		linked.distribute_lea_tm = a.distribute_lea_tm;
		linked.distribute_arr_tm = b.distribute_arr_tm;
		linked.distance = a.distance + b.distance;
		linked.wait_cost = Solution.formatDouble(a.wait_cost + b.wait_cost + (double) (bLea - aArr) * 24 / 60.0);
		linked.trans_cost = a.trans_cost + b.trans_cost;
		linked.charge_cnt = a.charge_cnt + b.charge_cnt;
		linked.charge_cost = a.charge_cost + b.charge_cost;
		linked.fixed_use_cost = a.fixed_use_cost;
		linked.trans_code = FileOutput.transCode(0);
		linked.valid = true;
		linked.totalCost();
		return linked;
	}
}
