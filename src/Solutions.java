

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Solutions {
	public List<Solution> solutions = new ArrayList<>();
	
	public Solutions clone() {
		Solutions result = new Solutions();
		for (int i = 0; i < solutions.size(); i++) {
			result.solutions.add(solutions.get(i));
		}
		return result;
	}
	
	public void sortByOrder() {
		Collections.sort(solutions, new Comparator<Solution>() {
			@Override
			public int compare(Solution o1, Solution o2) {
				return Double.compare(o1.order, o2.order);
			}
		});
	}
	
	public void sortByDist() {
		Collections.sort(solutions, new Comparator<Solution>() {
			@Override
			public int compare(Solution o1, Solution o2) {
				return Double.compare(o1.distance, o2.distance);
			}
		});
	}

	public double totalCost() {
		double total = 0;
		for (Solution sol : solutions) {
			total += sol.total_cost;
		}
		return total;
	}
}
