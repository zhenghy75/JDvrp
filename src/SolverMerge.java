import java.util.ArrayList;
import java.util.List;

public class SolverMerge {
	private IOVehicles vehicles;
	private Solutions sols;
	private Solutions mergedSols;
	private double percent;

	public Solutions calculate(Solutions sols, double percent, boolean sortMethod) {
		this.vehicles = FileInput.vehicles;
		this.sols = sols;
		this.percent = percent;
		this.mergedSols = new Solutions();
		if (!sortMethod) {
			sols.sortByOrder();
		} else {
			sols.sortByDist();
		}
		mergeSolutions();
		transCode();
		return this.mergedSols;
	}

	private void transCode() {
		int i = 0;
		for (Solution sol : mergedSols.solutions) {
			i++;
			sol.trans_code = FileOutput.transCode(i);
		}
	}

	private void mergeSolutions() {
		int n = sols.solutions.size();
		boolean[] isMerged = new boolean[n];
		for (int i = 0; i < n; i++) {
			isMerged[i] = false;
		}
		boolean isMerging = false;
		Solution merging = new Solution();
		for (int i = 0; i < n; i++) {
			if (isMerged[i]) {
				continue;
			}
			double minCost = Double.MAX_VALUE;
			int minIndex = -1;
			Solution minSolution = new Solution();
			merging = (isMerging)? merging : sols.solutions.get(i);
			for (int j = i + 1; j < n; j++) {
				if (isMerged[j]) {
					continue;
				}
				Solution b = sols.solutions.get(j);
				Solution mergedSolution = mergeSolution(merging, b);
				if (mergedSolution == null) {
					continue;
				}
				if (mergedSolution.total_cost >= percent * (merging.total_cost + b.total_cost)) {
					continue;
				}
				if (mergedSolution.total_cost < minCost) {
					minCost = mergedSolution.total_cost;
					minSolution = mergedSolution;
					minIndex = j;
				}
			}
			if (minIndex > 0) {
				isMerging = true;
				isMerged[minIndex] = true;
				merging = minSolution;
				i--;
			} else {
				isMerging = false;
				mergedSols.solutions.add(merging);
			}
		}
	}

	private Solution mergeSolution(Solution a, Solution b) {
		IOVehicle vehicle = selectVehicle(a, b);
		if (vehicle == null) {
			return null;
		}
		Scheduler scheduler = SchedulerFactory.getScheduler();
		List<IONode> pois = getPois(a, b);
		Solution merged = scheduler.schedule(pois, vehicle, 0);
		if (merged == null) {
			return null;
		}
		if (!merged.valid) {
			return null;
		}
		return merged;
	}
	
	private List<IONode> getPois(Solution a, Solution b) {
		List<IONode> pois = new ArrayList<>();
		pois.addAll(a.getPoiNodes());
		pois.addAll(b.getPoiNodes());
		return pois;
	}

	private IOVehicle selectVehicle(Solution a, Solution b) {
		double weightA = a.totalWeight();
		double weightB = b.totalWeight();
		double volumnA = a.totalVolumn();
		double volumnB = b.totalVolumn();
		IOVehicle vehicle = vehicles.iveco;
		if (weightA + weightB > vehicles.truck.max_weight) {
			return null;
		} else if (weightA + weightB > vehicles.iveco.max_weight) {
			vehicle = vehicles.truck;
		}
		if (volumnA + volumnB > vehicles.truck.max_volume) {
			return null;
		} else if (volumnA + volumnB > vehicles.iveco.max_volume) {
			vehicle = vehicles.truck;
		}
		return vehicle;
	}
}
