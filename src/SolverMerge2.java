import java.util.ArrayList;
import java.util.List;

public class SolverMerge2 {
	private IOVehicles vehicles;
	private Solutions sols;
	private Solutions mergedSols;

	public Solutions calculate(Solutions sols) {
		this.vehicles = FileInput.vehicles;
		this.sols = sols;
		this.mergedSols = new Solutions();
		mergeSolutions();
		transCode();
		return this.mergedSols;
	}

	private void transCode() {
		int i = 0;
		for (Solution sol : mergedSols.solutions) {
			i++;
			sol.trans_code = FileOutput.transCode(i);
		}
	}

	private void mergeSolutions() {
		mergedSols = sols;
		while (true) {
			int n = mergedSols.solutions.size();
			boolean[] isMerged = new boolean[n];
			for (int i = 0; i < n; i++) {
				isMerged[i] = false;
			}
			double maxImprove = -1;
			int maxAIndex = -1, maxBIndex = -1;
			Solution maxSol = null;
			for (int i = 0; i < n - 1; i++) {
				if (isMerged[i]) {
					continue;
				}
				Solution a = mergedSols.solutions.get(i);
				for (int j = i + 1; j < n;j ++) {
					if (isMerged[j]) {
						continue;
					}
					Solution b = mergedSols.solutions.get(j);
					Solution merged = mergeSolution(a, b);
					if (merged == null) {
						continue;
					}
					double improve = a.total_cost + b.total_cost - merged.total_cost;
					if (improve <= 0.01) {
						continue;
					}
					if (improve > maxImprove) {
						maxImprove = improve;
						maxAIndex = i;
						maxBIndex = j;
						maxSol = merged;
					}
				}
			}
			if (maxImprove < 0) {
				break;
			}
			Solutions temp = new Solutions();
			temp.solutions.add(maxSol);
			for (int i = 0; i < n; i++) {
				if (i != maxAIndex && i != maxBIndex) {
					temp.solutions.add(mergedSols.solutions.get(i));
				}
			}
			mergedSols = temp;
		}
	}

	private Solution mergeSolution(Solution a, Solution b) {
		IOVehicle vehicle = selectVehicle(a, b);
		if (vehicle == null) {
			return null;
		}
		Scheduler scheduler = SchedulerFactory.getScheduler();
		List<IONode> pois = getPois(a, b);
		Solution merged = scheduler.schedule(pois, vehicle, 0);
		if (merged == null) {
			return null;
		}
		if (!merged.valid) {
			return null;
		}
		return merged;
	}

	private List<IONode> getPois(Solution a, Solution b) {
		List<IONode> pois = new ArrayList<>();
		pois.addAll(a.getPoiNodes());
		pois.addAll(b.getPoiNodes());
		return pois;
	}

	private IOVehicle selectVehicle(Solution a, Solution b) {
		double weightA = a.totalWeight();
		double weightB = b.totalWeight();
		double volumnA = a.totalVolumn();
		double volumnB = b.totalVolumn();
		IOVehicle vehicle = vehicles.iveco;
		if (weightA + weightB > vehicles.truck.max_weight) {
			return null;
		} else if (weightA + weightB > vehicles.iveco.max_weight) {
			vehicle = vehicles.truck;
		}
		if (volumnA + volumnB > vehicles.truck.max_volume) {
			return null;
		} else if (volumnA + volumnB > vehicles.iveco.max_volume) {
			vehicle = vehicles.truck;
		}
		return vehicle;
	}
}
