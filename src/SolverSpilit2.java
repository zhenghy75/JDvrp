import java.util.ArrayList;
import java.util.List;

public class SolverSpilit2 {
	private IOVehicles vehicles;
	private Solutions newSols;
	private Solutions sols;

	public Solutions calculate(Solutions sols) {
		this.sols = sols;
		this.vehicles = FileInput.vehicles;
		int n = this.sols.solutions.size();
		SolverMerge2 solverMerge2 = new SolverMerge2();
		for (int i = 0; i < n; i++) {
			Solution sol = this.sols.solutions.get(i);
			int m = sol.getPoiNodesCount();
			for (int j = 0; j < m; j++) {
				newSols = new Solutions();
				List<Solution> spilit = spilitSol(sol, j);
				if (spilit != null && spilit.size() > 1) {
					newSols.solutions.addAll(spilit);
					for (int k = 0; k < n; k++) {
						if (k == i) {
							continue;
						}
						newSols.solutions.add(this.sols.solutions.get(k));
					}
					newSols = solverMerge2.calculate(newSols);
					if (newSols.totalCost() < this.sols.totalCost()) {
						System.out.println("find: " + i + ", " + j);
						System.out.println("total cost now = " + newSols.totalCost());
						this.sols = newSols;
						n = this.sols.solutions.size();
						i = -1;
						break;
					}
				}
			}
		}
		return this.sols;
	}

	private List<Solution> spilitSol(Solution sol, int index) {
		List<Solution> spilitSol = new ArrayList<>();
		List<IONode> poiNodes = sol.getPoiNodes();
		int n = poiNodes.size();
		if (n == 1) {
			spilitSol.add(sol);
			return spilitSol;
		}
		Scheduler scheduler = SchedulerFactory.getScheduler();
		IOVehicle vehicle = (sol.vehicle_type == 1) ? vehicles.iveco : vehicles.truck;
		List<IONode> poiNodesPart = new ArrayList<>(n - 1);
		for (int j = 0; j < n; j++) {
			if (j == index) {
				continue;
			}
			poiNodesPart.add(poiNodes.get(j));
		}
		Solution part = scheduler.schedule(poiNodesPart, vehicle, 0);
		if (part == null || !part.valid) {
			return null;
		}
		List<IONode> single = new ArrayList<>();
		single.add(poiNodes.get(index));
		Solution s = scheduler.schedule(single, vehicle, 0);
		spilitSol.add(part);
		spilitSol.add(s);
		return spilitSol;
	}

}
