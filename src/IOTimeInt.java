

public class IOTimeInt {
	public static int Time2Int(String time) {
		String[] s = time.split(":");
		int result = Integer.parseInt(s[0]) * 60 + Integer.parseInt(s[1]);
		return result;
	}
	public static String Int2Time(int time) {
		int minute = time % 60;
		int hour = (time - minute) / 60;
		String minS = (minute >= 10) ? Integer.toString(minute) : "0" + Integer.toString(minute);
		String hourS = (hour >= 10)? Integer.toString(hour) : "0" + Integer.toString(hour);
		String s = hourS + ":" + minS;
		return s;
	}
}
