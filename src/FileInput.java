import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileInput {
	public static IONodes nodes = new IONodes();
	public static IOVehicles vehicles = new IOVehicles();
	public static IODistTime distanceTimes;
	public static Solutions solutions = new Solutions();
	
	public static void loadSolutions(String fileName) throws NumberFormatException, IOException {
		int i = 0;
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		while (true) {
			String s = reader.readLine();
			if (s == null) {
				break;
			}
			if (i == 0) {
				i++;
				continue;
			}
			String[] str = s.split(",|\t");
			Solution sol = new Solution();
			sol.trans_code = str[0];
			sol.vehicle_type = Integer.parseInt(str[1]);
			sol.dist_seq = str[2];
			sol.distribute_lea_tm = str[3];
			sol.distribute_arr_tm = str[4];
			sol.distance = Integer.parseInt(str[5]);
			sol.trans_cost = Double.parseDouble(str[6]);
			sol.charge_cost = Integer.parseInt(str[7]);
			sol.wait_cost = Double.parseDouble(str[8]);
			sol.fixed_use_cost = Integer.parseInt(str[9]);
			sol.total_cost = Double.parseDouble(str[10]);
			sol.charge_cnt = Integer.parseInt(str[11]);
			sol.seqDist();
			sol.valid = true;
			sol.order = 0;
			solutions.solutions.add(sol);
		}
		reader.close();
	}
	
	public static void init() throws NumberFormatException, IOException {
		nodeInput("./data/input_node.txt");
		vehicleInput("./data/input_vehicle_type.txt");
		int n = nodes.pois.size() + nodes.chargers.size() + 10;
		distanceTimes = new IODistTime(n);
		distanceTimeInput("./data/input_distance-time.txt");
		System.out.println("input OK");
	}
	
	public static void solutionsInput(String fileName) throws NumberFormatException, IOException {
		int i = 0;
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		while (true) {
			String s = reader.readLine();
			if (s == null) {
				break;
			}
			if (i == 0) {
				i++;
				continue;
			}
			String[] str = s.split("\t");
			int id = Integer.parseInt(str[0]);
			int type = Integer.parseInt(str[1]);
			double lat = Double.parseDouble(str[2]);
			double lng = Double.parseDouble(str[3]);
			double pack_total_weight = ("-".equals(str[4]))? 0 : Double.parseDouble(str[4]);
			double pack_total_volume = ("-".equals(str[5]))? 0 : Double.parseDouble(str[5]);
			String first_receive_tm = ("-".equals(str[6]))? "0:0" : str[6];
			String last_receive_tm = ("-".equals(str[7]))? "0:0" : str[7];
			switch (type) {
			case 1:
				nodes.center = new IONode(id, type, lat, lng, pack_total_weight, pack_total_volume, first_receive_tm,
						last_receive_tm);
				break;
			case 2:
				nodes.pois.add(new IONode(id, type, lat, lng, pack_total_weight, pack_total_volume, first_receive_tm,
						last_receive_tm));
				break;
			case 3:
				nodes.chargers.add(new IONode(id, type, lat, lng, pack_total_weight, pack_total_volume, first_receive_tm,
						last_receive_tm));
				break;
			}
		}
		reader.close();
	}

	public static void nodeInput(String fileName) throws NumberFormatException, IOException {
		int i = 0;
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		while (true) {
			String s = reader.readLine();
			if (s == null) {
				break;
			}
			if (i == 0) {
				i++;
				continue;
			}
			String[] str = s.split("\t");
			int id = Integer.parseInt(str[0]);
			int type = Integer.parseInt(str[1]);
			double lat = Double.parseDouble(str[2]);
			double lng = Double.parseDouble(str[3]);
			double pack_total_weight = ("-".equals(str[4]))? 0 : Double.parseDouble(str[4]);
			double pack_total_volume = ("-".equals(str[5]))? 0 : Double.parseDouble(str[5]);
			String first_receive_tm = ("-".equals(str[6]))? "0:0" : str[6];
			String last_receive_tm = ("-".equals(str[7]))? "0:0" : str[7];
			switch (type) {
			case 1:
				nodes.center = new IONode(id, type, lat, lng, pack_total_weight, pack_total_volume, first_receive_tm,
						last_receive_tm);
				break;
			case 2:
				nodes.pois.add(new IONode(id, type, lat, lng, pack_total_weight, pack_total_volume, first_receive_tm,
						last_receive_tm));
				break;
			case 3:
				nodes.chargers.add(new IONode(id, type, lat, lng, pack_total_weight, pack_total_volume, first_receive_tm,
						last_receive_tm));
				break;
			}
		}
		reader.close();
	}

	public static void vehicleInput(String fileName) throws NumberFormatException, IOException {
		int i = 0;
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		while (true) {
			String s = reader.readLine();
			if (s == null) {
				break;
			}
			if (i == 0) {
				i++;
				continue;
			}
			String[] str = s.split("\t");
			int id = Integer.parseInt(str[0]);
			String name = str[1];
			double max_volume = Double.parseDouble(str[2]);
			double max_weight = Double.parseDouble(str[3]);
			int vehicle_cnt = 0;
			int driving_range = Integer.parseInt(str[5]);
			double charge_tm = Double.parseDouble(str[6]);
			double unit_trans_cost = Double.parseDouble(str[7]);
			int vehicle_cost = Integer.parseInt(str[8]);
			switch (id) {
			case 1:
				vehicles.iveco = new IOVehicle(id, name, max_volume, max_weight, vehicle_cnt, driving_range, charge_tm,
						unit_trans_cost, vehicle_cost);
				break;
			case 2:
				vehicles.truck = new IOVehicle(id, name, max_volume, max_weight, vehicle_cnt, driving_range, charge_tm,
						unit_trans_cost, vehicle_cost);
				break;
			}
		}
		reader.close();
	}
	
	public static void distanceTimeInput(String fileName) throws NumberFormatException, IOException {
		int i = 0;
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		while (true) {
			String s = reader.readLine();
			if (s == null) {
				break;
			}
			if (i == 0) {
				i++;
				continue;
			}
			String[] str = s.split(",");
			int from_node =  Integer.parseInt(str[1]);
			int to_node = Integer.parseInt(str[2]);
			int distance = Integer.parseInt(str[3]);
			int spend_tm = Integer.parseInt(str[4]);
			distanceTimes.distances[from_node][to_node] = distance;
			distanceTimes.distances[to_node][from_node] = distance;
			distanceTimes.times[from_node][to_node] = spend_tm;
			distanceTimes.times[to_node][from_node] = spend_tm;
		}
		reader.close();
	}
}
