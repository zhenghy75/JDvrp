import java.util.ArrayList;
import java.util.List;

public class Solution {
	public String trans_code;
	public int vehicle_type;
	public String dist_seq = new String();
	public String distribute_lea_tm;
	public String distribute_arr_tm;
	public int distance;
	public double trans_cost;
	public int charge_cost;
	public double wait_cost = 0;
	public int fixed_use_cost;
	public double total_cost;
	public int charge_cnt;
	public List<Integer> seq = new ArrayList<>();
	public boolean valid = true;
	public double order;
	
	public Solution clone() {
		Solution sol = new Solution();
		sol.trans_code = this.trans_code;
		sol.vehicle_type = this.vehicle_type;
		sol.dist_seq = this.dist_seq;
		sol.distribute_lea_tm = this.distribute_lea_tm;
		sol.distribute_arr_tm = this.distribute_arr_tm;
		sol.distance = this.distance;
		sol.trans_cost = this.trans_cost;
		sol.charge_cost = this.charge_cost;
		sol.wait_cost = this.wait_cost;
		sol.fixed_use_cost = this.fixed_use_cost;
		sol.total_cost = this.total_cost;
		sol.charge_cnt = this.charge_cnt;
		sol.seq = new ArrayList<>();
		for (Integer i : this.seq) {
			sol.seq.add(i);
		}
		sol.valid = this.valid;
		return sol;
	}
	
	public double totalWeight() {
		double weight = 0;
		for (IONode poi : getPoiNodes()) {
			weight += poi.pack_total_weight;
		}
		return weight;
	}
	
	public double totalVolumn() {
		double volume = 0;
		for (IONode poi : getPoiNodes()) {
			volume += poi.pack_total_volume;
		}
		return volume;
	}
	
	public List<IONode> getPoiNodes() {
		List<IONode> poiNodes = new ArrayList<>();
		for (int i = 0; i < seq.size(); i++) {
			int j = seq.get(i);
			if (j > 0 && j < 1001) {
				poiNodes.add(FileInput.nodes.pois.get(j - 1));
			}
		}
		return poiNodes;
	}
	
	public int getPoiNodesCount() {
		int count = 0;
		for (int i = 0; i < seq.size(); i++) {
			int j = seq.get(i);
			if (j > 0 && j < 1001) {
				count++;
			}
		}
		return count;
	}

	public void constructRoute(int carNum, IOVehicle vehicle) {
		int startTime = getStartTime();
		trans_code = FileOutput.transCode(carNum);
		vehicle_type = vehicle.id;
		dist_seq = distSeq(seq);
		distribute_lea_tm = IOTimeInt.Int2Time(startTime);
		int endTime = endTime(startTime);
		if (endTime < 0 || endTime > 1440) {
			valid = false;
			return;
		}
		distribute_arr_tm = IOTimeInt.Int2Time(endTime);
		distance(vehicle);
		if (valid) {
			trans_cost = formatDouble(distance * vehicle.unit_trans_cost / 1000);
			chargeCount();
			charge_cost = charge_cnt * 50;
			fixed_use_cost = vehicle.vehicle_cost;
			totalCost();
		}
	}
	
	private int getStartTime() {
		int centerNodeTime = FileInput.distanceTimes.getTime(seq.get(0), seq.get(1));
		int startTime = FileInput.nodes.pois.get(seq.get(1) - 1).first_receive_tm - centerNodeTime;
		startTime = (startTime >= 480) ? startTime : 480;
		return startTime;
	}
	
	public static double formatDouble(double d) {
		return (double) Math.round(d * 100) / 100;
	}
	
	private void distance(IOVehicle vehicle) {
		distance = 0;
		int usedDist = distance;
		for (int i = 0; i < seq.size() - 1; i++) {
			int seqId = seq.get(i);
			int step = FileInput.distanceTimes.getDistance(seqId, seq.get(i + 1));
			distance += step;
			if (seqId <= 1000) {
				usedDist += step;
			} else {
				usedDist = step;
			}
			if (usedDist > vehicle.driving_range) {
				valid = false;
				return;
			}
		}
	}
	
	private void chargeCount() {
		int count = 0;
		for (Integer i : seq) {
			if (i > 1000) {
				count++;
			}
		}
		charge_cnt = count;
	}

	private int endTime(int startTime) {
		int endTime = startTime;
		for (int i = 0; i < seq.size() - 1; i++) {
			int seqId = seq.get(i);
			if (seqId > 0 && seqId <= 1000) {
				int firstReceiveTime = FileInput.nodes.pois.get(seqId - 1).first_receive_tm;
				int lastReceiveTime = FileInput.nodes.pois.get(seqId - 1).last_receive_tm;
				if (firstReceiveTime > endTime) {
					wait_cost += (double) (firstReceiveTime - endTime) * 24 / 60.0;
					endTime = firstReceiveTime;
				}
				if (lastReceiveTime < endTime) {
					return -1;
				}
			}
			endTime += FileInput.distanceTimes.getTime(seqId, seq.get(i + 1));
			endTime += (seqId > 0) ? 30 : 0;
		}
		wait_cost = formatDouble(wait_cost);
		return endTime;
	}

	public String distSeq(List<Integer> seq) {
		String dist_seq = "";
		for (int i = 0; i < seq.size() - 1; i++) {
			dist_seq += Integer.toString(seq.get(i)) + ";";
		}
		dist_seq += Integer.toString(seq.get(seq.size() - 1));
		return dist_seq;
	}
	
	public void seqDist() {
		seq = new ArrayList<>();
		String[] str = dist_seq.split(";");
		for (int i = 0; i < str.length; i++) {
			seq.add(Integer.parseInt(str[i]));
		}
	}

	public void totalCost() {
		total_cost = formatDouble(trans_cost + charge_cost + wait_cost + fixed_use_cost);
	}

	public String toFileString() {
		String s = trans_code + ",";
		s += Integer.toString(vehicle_type) + ",";
		s += dist_seq + ",";
		s += distribute_lea_tm + ",";
		s += distribute_arr_tm + ",";
		s += Integer.toString(distance) + ",";
		s += Double.toString(trans_cost) + ",";
		s += Integer.toString(charge_cost) + ",";
		s += Double.toString(wait_cost) + ",";
		s += Integer.toString(fixed_use_cost) + ",";
		s += Double.toString(total_cost) + ",";
		s += Integer.toString(charge_cnt);
		s += "\n";
		return s;
	}
}
