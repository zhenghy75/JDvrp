import java.util.ArrayList;
import java.util.List;

public class SolverSpilit3 {
	private IOVehicles vehicles;
	private Solutions newSols;
	private Solutions sols;

	public Solutions calculate(Solutions sols) {
		this.sols = sols;
		this.vehicles = FileInput.vehicles;
		int n = this.sols.solutions.size();
		SolverMerge2 solverMerge2 = new SolverMerge2();
		for (int i = 0; i < n; i++) {
			Solution sol = this.sols.solutions.get(i);
			newSols = new Solutions();
			List<Solution> spilit = spilitSol(sol);
			if (spilit != null && spilit.size() > 1) {
				newSols.solutions.addAll(spilit);
				for (int j = 0; j < n; j++) {
					if (j == i) {
						continue;
					}
					newSols.solutions.add(this.sols.solutions.get(j));
				}
				newSols = solverMerge2.calculate(newSols);
				if (newSols.totalCost() < this.sols.totalCost()) {
					System.out.println("find: " + i);
					System.out.println("total cost now = " + newSols.totalCost());
					this.sols = newSols;
					n = this.sols.solutions.size();
					i = -1;
				}
			}
		}
		return this.sols;
	}

	private List<Solution> spilitSol(Solution sol) {
		List<Solution> spilitSol = new ArrayList<>();
		List<IONode> poiNodes = sol.getPoiNodes();
		int n = poiNodes.size();
		if (n == 1) {
			spilitSol.add(sol);
			return spilitSol;
		}
		Scheduler scheduler = SchedulerFactory.getScheduler();
		IOVehicle vehicle = (sol.vehicle_type == 1) ? vehicles.iveco : vehicles.truck;
		for (int i = 0; i < n; i++) {
			List<IONode> poiNodesPart = new ArrayList<>(1);
			poiNodesPart.add(poiNodes.get(i));
			Solution s = scheduler.schedule(poiNodesPart, vehicle, 0);
			spilitSol.add(s);
		}
		return spilitSol;
	}

}
