import java.io.IOException;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) throws NumberFormatException, IOException {
		Solutions sols = new Solutions();
		FileInput.init();
//		sols = simple();
//		sols = merge(sols);
//		sols = merge2(sols);
//		sols = ga(sols);
		FileInput.loadSolutions("./result/ResultMerge2.csv");
		sols = FileInput.solutions;
//		sols = spilit(sols);
		sols = spilit2(sols);
//		sols = spilit3(sols);
		sols = link(sols);
		System.out.println("Total cost = " + sols.totalCost());
		FileOutput.writeFile(sols, "./result/Result.csv");
//		drawMap(sols);
	}

	public static Solutions simple() {
		SolverSimple solver = new SolverSimple();
		Solutions sols = solver.calculate();
		return sols;
	}
	
	public static Solutions merge(Solutions sols) {
		SolverMerge solverMerge = new SolverMerge();
		for (int i = 0; i < 5; i++) {
			double pencent = (double) i * 0.1 + 0.6;
			sols = solverMerge.calculate(sols, pencent, true);
		}
		return sols;
	}
	
	public static Solutions merge2(Solutions sols) {
		SolverMerge2 solverMerge2 = new SolverMerge2();
		sols = solverMerge2.calculate(sols);
		return sols;
	}
	
	public static Solutions ga(Solutions sols) {
		SolverGA ga = new SolverGA();
		return ga.calculate(sols);
	}
	
	public static Solutions spilit(Solutions sols) {
		SolverSpilit solverSpilit = new SolverSpilit();
		SolverMerge2 solverMerge2 = new SolverMerge2();
		for (int i = 0; i < 1; i++) {
			Solutions spilitSols = solverSpilit.calculate(sols);
			Solutions merged = solverMerge2.calculate(spilitSols);
			double mergedTotal = merged.totalCost();
			double solsTotal = sols.totalCost();
			System.out.println("costs = " + mergedTotal + ", " + solsTotal);
			if (mergedTotal < solsTotal) {
				sols = merged;
			}
		}
		return sols;
	}
	
	public static Solutions spilit2(Solutions sols) {
		SolverSpilit2 solverSpilit2 = new SolverSpilit2();
		return solverSpilit2.calculate(sols);
	}
	
	private static Solutions spilit3(Solutions sols) {
		SolverSpilit3 solverSpilit3 = new SolverSpilit3();
		return solverSpilit3.calculate(sols);
	}
	
	public static Solutions link(Solutions sols) {
		SolverLink solverLink = new SolverLink();
		Solutions linked = solverLink.calculate(sols);
		return linked;
	}
	
	public static void drawMap(Solutions sols) {
		DrawMap frame = new DrawMap(sols);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
	}
}
