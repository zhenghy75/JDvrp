import java.util.ArrayList;
import java.util.List;

public class SolverSpilit {
	private IOVehicles vehicles;
	private Solutions newSols;
	private Solutions sols;

	public Solutions calculate(Solutions sols) {
		this.sols = sols;
		this.vehicles = FileInput.vehicles;
		this.newSols = new Solutions();
		for (int i = 0; i < this.sols.solutions.size(); i++) {
			Solution sol = this.sols.solutions.get(i);
			List<Solution> spilit = spilitSol(sol);
			newSols.solutions.addAll(spilit);
		}
		System.out.println("sols size = " + sols.solutions.size() + ", new size = " + newSols.solutions.size());
		return this.newSols;
	}

	private List<Solution> spilitSol(Solution sol) {
		List<Solution> spilitSol = new ArrayList<>();
		if (sol.getPoiNodesCount() < 3) {
			spilitSol.add(sol);
			return spilitSol;
		}
		List<IONode> poiNodes = sol.getPoiNodes();
		int n = poiNodes.size();
		double maxImprovement = -1;
		Solution s1 = null, s2 = null;
		int maxIndex = -1;
		Scheduler scheduler = SchedulerFactory.getScheduler();
		IOVehicle vehicle = (sol.vehicle_type == 1) ? vehicles.iveco : vehicles.truck;
		for (int i = 0; i < n; i++) {
			List<IONode> poiNodesPart = new ArrayList<>(n - 1);
			for (int j = 0; j < n; j++) {
				if (j == i) {
					continue;
				}
				poiNodesPart.add(poiNodes.get(j));
			}
			Solution part = scheduler.schedule(poiNodesPart, vehicle, 0);
			if (part == null || !part.valid) {
				continue;
			}
			List<IONode> single = new ArrayList<>();
			single.add(poiNodes.get(i));
			Solution s = scheduler.schedule(single, vehicle, 0);
			double improvement = s.total_cost + part.total_cost - sol.total_cost;
			if (improvement > 0 && improvement > maxImprovement) {
				maxImprovement = improvement;
				s1 = s;
				s2 = part;
			}
		}
		if (maxIndex < 0) {
			spilitSol.add(sol);
			return spilitSol;
		}
		System.out.println("find");
		spilitSol.add(s1);
		spilitSol.add(s2);
		return spilitSol;
	}

}
