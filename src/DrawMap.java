import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DrawMap extends JFrame {
	Solutions sols;

	public DrawMap(Solutions sols) {
		this.sols = sols;
		this.setTitle("Draw a Polygon");
		this.setSize(800, 800);
		DrawPanel panel = new DrawPanel();
		this.add(panel);
	}

	class DrawPanel extends JPanel {
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			int k = 0;
			for (Solution sol : sols.solutions) {
				Polygon po = new Polygon();
				int n = sol.seq.size();
				double[] x = new double[n], y = new double[n];
				for (int i = 0; i < n; i++) {
					IONode node = null;
					int j = sol.seq.get(i);
					if (j == 0) {
						node = FileInput.nodes.center;
					} else if (j > 0 && j < 1001) {
						node = FileInput.nodes.pois.get(j - 1);
					} else {
						node = FileInput.nodes.chargers.get(j - 1001);
					}
					x[i] = (node.lat - 116) * 1000;
					y[i] = (node.lng - 39.5) * 1000;
				}
				for (int i = 0; i < n; i++) {
					po.addPoint((int) x[i], (int) y[i]);
				}
				if (k < 100 || k >= 110) {
					k++;
					continue;
				}
				Color[] colors = {Color.BLACK, Color.BLUE, Color.RED, Color.GREEN, Color.CYAN};
				g.setColor(colors[k % 5]);
				k++;
				g.drawPolygon(po);
			}
			g.dispose();
		}
	}
}