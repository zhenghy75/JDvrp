import java.util.List;

public abstract class Scheduler {
	public abstract Solution schedule(List<IONode> pois, IOVehicle vehicle, int carNum);
}
